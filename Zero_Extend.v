// implemented by 0016002 舒俊維 

module Zero_Extend(
    data_i,
    data_o
    );

parameter size = 16;

//I/O ports
input   [size-1:0] data_i;
output  [32-1:0] data_o;

//Internal Signals
//reg     [32-1:0] data_o;

//Zero extended
assign data_o = { {(32-size){1'b0}}, data_i};
          
endmodule      
     
