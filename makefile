# Verilog Compiler
VC = iverilog
TOPMODULE = Simple_Single_CPU
SUBMODULE_FILES = ProgramCounter.v Instr_Memory.v Reg_File.v MUX_2to1.v Decoder.v ALU_Ctrl.v Sign_Extend.v Zero_Extend.v ${PC_MODULE_FILES} ${ALU_MODULE_FILES}
TESTER_MODULE = Test_Bench
TESTER = ${TESTER_MODULE}.v
VFLAGS = -s ${TESTER_MODULE}

PC_MODULE_FILES = Adder.v Shift_Left_Two_32.v
ALU_MODULE_FILES = alu.v alu_top.v FA_1b.v

all: ${TOPMODULE}.vvp
	
# simulate verilog module
simulate: ${TOPMODULE}.vvp
	vvp $<

# look at waveform
waveform: ${TOPMODULE}.vcd
	gtkwave $<

# compile verilog module
${TOPMODULE}.vvp: ${TOPMODULE}.v ${TESTER} ${SUBMODULE_FILES}
	${VC} ${VFLAGS} -o $@ $^
# iverilog -s TESTER_MODULE -o TOPMODULE.vvp TOPMODULE.v TESTER SUBMODULE_FILES

clean:
	rm -f *.vvp *.vcd

.PHONY: clean simulate waveform 
