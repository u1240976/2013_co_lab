// implemented by 0016002 舒俊維 

module Simple_Single_CPU(
        clk_i,
        rst_n
        );

//I/O port
input         clk_i;
input         rst_n;

//Internal Signals

wire [32-1:0] pc_out;
wire [32-1:0] instruction;
wire [5-1:0] reg_write_addr;
wire [32-1:0] reg_out1;
wire [32-1:0] reg_out2;

wire [32-1:0] shamt_ex;

wire [32-1:0] SE_immed;
wire [32-1:0] ZE_immed;
wire [32-1:0] immed_32b;
wire [32-1:0] alu_in2_tmp;
wire [32-1:0] alu_in2;
wire [32-1:0] alu_in1;
wire [32-1:0] alu_out;
wire alu_zero;
wire alu_carry;
wire alu_overflow;

wire [32-1:0] branch_addr_ch;
wire [32-1:0] branch_addr;

wire [32-1:0] pc_add_4;
wire [32-1:0] pc_next;
//control signal
wire reg_dst;
wire is_write_reg;
wire immed_exten;
wire alu_in2_src;
wire alu_in_src_shamt;
wire is_branch;
wire pc_src;

wire [6-1:0] instr_op_2;
wire [4-1:0] alu_op;

//Constants
parameter OP_S = 31, OP_E = 26, RS_S = 25, RS_E = 21, RT_S = 20, RT_E = 16, RD_S = 15, RD_E = 11,
          SHAMT_S = 10, SHAMT_E = 6, FUNC_S = 5, FUNC_E = 0,
          I_ADDR_S = 15, I_ADDR_E = 0;

//Greate componentes
ProgramCounter PC(
        .clk_i(clk_i),
        .rst_n(rst_n),
        .pc_in_i(pc_next),
        .pc_out_o(pc_out)
        );

// Program Counter control
Adder Adder1(
        .src1_i(pc_out),
        .src2_i(32'd4),
        .sum_o(pc_add_4)
        );

Shift_Left_Two_32 Shifter(
        .data_i(SE_immed),
        .data_o(branch_addr_ch)
        );

Adder Adder2(
        .src1_i(pc_add_4),
        .src2_i(branch_addr_ch),
        .sum_o(branch_addr)
        );

assign pc_src = is_branch & alu_zero;

MUX_2to1 #(.size(32)) Mux_PC_Source(
        .data0_i(pc_add_4),
        .data1_i(branch_addr),
        .select_i(pc_src),
        .data_o(pc_next)
        );
// end

Instr_Memory IM(
        .pc_addr_i(pc_out),
        .instr_o(instruction)
        );

MUX_2to1 #(.size(5)) Mux_Write_Reg(
        .data0_i(instruction[RT_S:RT_E]),
        .data1_i(instruction[RD_S:RD_E]),
        .select_i(reg_dst),
        .data_o(reg_write_addr)
        );

Reg_File RF(
        .clk_i(clk_i),
        .rst_n(rst_n) ,
        .RSaddr_i(instruction[RS_S:RS_E]) ,
        .RTaddr_i(instruction[RT_S:RT_E]),
        .RDaddr_i(reg_write_addr),
        .RDdata_i(alu_out),
        .RegWrite_i(is_write_reg),
        .RSdata_o(reg_out1),
        .RTdata_o(reg_out2)
        );

Decoder Decoder(
        .instr_op_i(instruction[OP_S:OP_E]),
        .RegWrite_o(is_write_reg),
        .ALU_op_o(instr_op_2),
        .ALUSrc_o(alu_in2_src),
        .RegDst_o(reg_dst),
        .Branch_o(is_branch),
        .immed_exten(immed_exten)
        );

ALU_Ctrl AC(
        .funct_i(instruction[FUNC_S:FUNC_E]),
        .ALUOp_i(instr_op_2),
        .ALUCtrl_o(alu_op),
        .ALUSrc_shamt_o(alu_in_src_shamt)
        );

Sign_Extend SE(
        .data_i(instruction[I_ADDR_S:I_ADDR_E]),
        .data_o(SE_immed)
        );

Zero_Extend #(.size(16)) ZE(
        .data_i(instruction[I_ADDR_S:I_ADDR_E]),
        .data_o(ZE_immed)
        );

MUX_2to1 #(.size(32)) Mux_extend_immed(
        .data0_i(SE_immed),
        .data1_i(ZE_immed),
        .select_i(immed_exten),
        .data_o(immed_32b)
        );

MUX_2to1 #(.size(32)) Mux_ALUSrc(
        .data0_i(reg_out2),
        .data1_i(immed_32b),
        .select_i(alu_in2_src),
        .data_o(alu_in2)
        );

Zero_Extend #(.size(5)) ZE_shift(
        .data_i(instruction[SHAMT_S:SHAMT_E]),
        .data_o(shamt_ex)
        );

MUX_2to1 #(.size(32)) Mux_ALUSrc1(
        .data0_i(reg_out1),
        .data1_i(shamt_ex),
        .select_i(alu_in_src_shamt),
        .data_o(alu_in1)
        );

alu ALU(
        .rst_n(rst_n),
        .src1(alu_in1),
        .src2(alu_in2),
        .ALU_control(alu_op),
        .result(alu_out),
        .zero(alu_zero),
        .cout(alu_carry),
        .overflow(alu_overflow)
        );

endmodule



